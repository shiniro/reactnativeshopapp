import React, { useState, useCallback, useEffect, useReducer } from 'react';
import { ScrollView, View, StyleSheet, Platform, Alert, KeyboardAvoidingView, ActivityIndicator } from 'react-native';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import { useSelector, useDispatch } from 'react-redux';

import Input from '../../components/UI/Input';
import HeaderButton from '../../components/UI/HeaderButton';
import * as productsActions from '../../store/actions/products';
import Colors from '../../constants/Colors';

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';

const formReducer = (state, action) => {
  switch(action.type) {
    case FORM_INPUT_UPDATE:
      const updatedValues = {
        ...state.inputValues,
        [action.input]: action.value
      };

      const updatedValidities = {
        ...state.inputValidities,
        [action.input]: action.isValid
      };

      let updatedFormIsValid = true;
      for (const key in updatedValidities) {
        updatedFormIsValid = updatedFormIsValid && updatedValidities[key];
      }

      return {
        inputValues: updatedValues,
        inputValidities: updatedValidities,
        formIsValid: updatedFormIsValid
      }

    default:
      return state;
  }
};


const EditProductScreen = props => {

  const dispatch = useDispatch();

  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState();

  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      title: editProduct ? editProduct.title : '',
      imageUrl: editProduct ? editProduct.imageUrl : '',
      description: editProduct ? editProduct.description : '',
      price: ''
    },
    inputValidities: {
      title: editProduct ? true : false,
      imageUrl: editProduct ? true : false,
      description: editProduct ? true : false,
      price: editProduct ? true : false,
    },
    formIsValid: editProduct ? true : false,
  }); // second argument is the initial state

  const prodId = props.route.params ? props.route.params.productId : null;
  const editProduct = useSelector(state => state.products.userProducts.find(product => product.id === prodId));

  // const[title, setTitle] = useState(editProduct ? editProduct.title : '');
  // const [titleIsValid, setTitleIsValid] = useState(false);
  // const[imageUrl, setImageURL] = useState(editProduct ? editProduct.imageUrl : '');
  // const[price, setPrice] = useState('');
  // const[description, setDecription] = useState(editProduct ? editProduct.description : '');


  const submitHandler = useCallback(async () => {
    if (!formState.formIsValid) {
      Alert.alert('Wrong input !', 'Please check the error in the form', [{ text: 'Okay' }]);
      return;
    }

    setError(null);
    setIsLoading(true);

    try {
      if(editProduct) {
        await dispatch(productsActions.updateProduct(prodId, formState.inputValues.title, formState.inputValues.description, formState.inputValues.imageUrl));
      } else {
        await dispatch(productsActions.createProduct(formState.inputValues.title, formState.inputValues.description, formState.inputValues.imageUrl, +formState.inputValues.price));
      }
      props.navigation.goBack();
    } catch (error) {
      setError(error);
    }

    setIsLoading(false);

  }, [dispatch, prodId, formState]);


  const inputChangeHandler = useCallback((inputIdentifier, inputValue, inputValidity) => {
    dispatchFormState({
      type: FORM_INPUT_UPDATE,
      value: inputValue,
      isValid: inputValidity,
      input: inputIdentifier
    });
  }, [dispatchFormState]);

  useEffect(() => {
    if (error) {
      Alert.alert('An error occured', error, [{ text: 'Okay' }]);
    }
  }, [error])

  useEffect(() => {
    props.navigation.setOptions({
      headerRight: () => (<HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item title="Save" iconName={ Platform.OS === 'android' ? 'md-checkmark' : 'ios-checkmark' } onPress={submitHandler}></Item>
      </HeaderButtons>)
    });
  }, [submitHandler]);

  if(isLoading) {
    return (
      <View style={styles.centered}>
        <ActivityIndicator 
          size="large"
          color={Colors.primary}
        />
      </View>
    )
  }

  return (
    <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" keyboardVerticalOffset={100}>
      <ScrollView>
        <View style={styles.form}>
          <Input
            id="title"
            label="Title"
            errorText="Please enter a valid title!"
            keyboardType="default"
            autoCapitalize="sentences"
            autoCorrect
            returnKeyType="next"
            onInputChange={inputChangeHandler}
            initialValue={editProduct ? editProduct.title : ''}
            intiallyValid={!!editProduct}
            required
            />
          <Input
            id="imageUrl"
            label="Image URL"
            errorText="Please enter a valid image url!"
            autoCorrect
            returnKeyType="next"
            onInputChange={inputChangeHandler}
            initialValue={editProduct ? editProduct.imageUrl : ''}
            intiallyValid={!!editProduct}
            required
            />
          { editProduct 
            ? null 
            : <Input
              id="price"
              label="Price"
              errorText="Please enter a valid price!"
              keyboardType="decimal-pad"
              returnKeyType="next"
              onInputChange={inputChangeHandler}
              required
              min={0.1}
              /> }
          <Input
            id="description"
            label="Description"
            errorText="Please enter a valid description!"
            keyboardType="default"
            autoCapitalize="sentences"
            autoCorrect
            multiline
            numberOfLines={3}
            onInputChange={inputChangeHandler}
            initialValue={editProduct ? editProduct.description : ''}
            intiallyValid={!!editProduct}
            required
            minLength={5}
            />
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
}

export const screenOptions = navData => {

  const submitFn = navData.route.params ? navData.route.params.submit : null;
  
  return {
    headerTitle: navData.route.params && navData.route.params.productId ? 'Edit Product' : 'Add Product',
  }
};

const styles = StyleSheet.create({
  centered: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  form: {
    margin: 20
  }
});

export default EditProductScreen;