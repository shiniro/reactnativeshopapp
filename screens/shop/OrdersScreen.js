import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, StyleSheet, Platform, ActivityIndicator } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';

import HeaderButton from '../../components/UI/HeaderButton';
import OrderItem from '../../components/shop/OrderItem';
import * as OrdersActions from '../../store/actions/orders';
import Colors from '../../constants/Colors';

const OrderScreen = props => {

  const orders = useSelector(state => state.orders.orders);
  const dispatch = useDispatch();

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    dispatch(OrdersActions.fetchOrders())
    .then(() => {
      setIsLoading(false);
    });
  }, [dispatch]);

  if (isLoading) {
    return (
      <View style={styles.screen}>
        <ActivityIndicator
          size="large"
          color={Colors.primary}
        />
      </View>
    )
  }

  if(orders.length === 0) {
    return (
      <View style={styles.screen}>
        <Text>No order found, maybe start ordering some product ?</Text>
      </View>
    )
  }

  return (
    <FlatList data={orders} keyExtractor={item => item.id} renderItem={itemData => <OrderItem amount={itemData.item.totalAmount} date={itemData.item.readableDate} items={itemData.item.items} />} />
  );
}

export const screenOptions = navData => {
  return {
    headerTitle: 'Your Orders',
    headerLeft: () => (<HeaderButtons HeaderButtonComponent={HeaderButton}>
      <Item title="Menu" iconName={ Platform.OS === 'android' ? 'md-menu' : 'ios-menu' } onPress={() => {
        navData.navigation.toggleDrawer();
      }}></Item>
    </HeaderButtons>)
  };
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default OrderScreen;