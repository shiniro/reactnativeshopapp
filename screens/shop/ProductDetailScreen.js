import React from 'react';
import { ScrollView, View, Text, Image, Button, StyleSheet } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import Colors from '../../constants/Colors';
import * as cartActions from '../../store/actions/cart';


const ProductDetailScreen = props => {

  const dispatch = useDispatch();

  const productId = props.route.params ? props.route.params.productId : null;
  const selectedProduct = useSelector(state => state.products.availableProducts.find(prod => prod.id === productId));

  return (
    <ScrollView>
      <Image style={ styles.image } source={{ uri: selectedProduct.imageUrl }} />
      <View style={ styles.actions }><Button color={ Colors.primary } title="Add to the Cart" onPress={() => {
        dispatch(cartActions.addToCard(selectedProduct));
      }} /></View>
      <Text style={ styles.price }>${ selectedProduct.price.toFixed(2) }</Text>
      <Text style={ styles.description } >{ selectedProduct.description }</Text>
    </ScrollView>
  );
}

export const screenOptions = navData => {
  return {
    headerTitle: navData.route.params ? navData.route.params.productTitle : ''
  }
}

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: 300,
  },
  actions: {
    marginVertical: 10,
    alignItems: 'center'
  },
  price: {
    marginVertical: 20,
    fontSize: 20,
    color: '#888',
    textAlign: 'center',
    fontFamily: 'open-sans-bold'
  },
  description: {
    marginHorizontal: 20,
    fontSize: 14,
    textAlign: 'center',
    fontFamily: 'open-sans'
  }
});

export default ProductDetailScreen;