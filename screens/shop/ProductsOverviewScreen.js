import React, { useState, useEffect, useCallback } from 'react';
import { View, Text, FlatList, Button, Platform, StyleSheet, ActivityIndicator } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';

import ProductItem from '../../components/shop/ProductItem';
import HeaderButton from '../../components/UI/HeaderButton';

import Colors from '../../constants/Colors';

import * as cartActions from '../../store/actions/cart';
import * as productsActions from '../../store/actions/products';

const ProductsOverviewScreen = props => {

  const dispatch = useDispatch();

  const [isLoading, setIsLoading] = useState(false);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [error, setError] = useState();
  const products = useSelector(state => state.products.availableProducts);

  const loadProducts = useCallback(async () => { // to be ale to use async this way need to wrap everything in a function or else use then
    setError(null);
    setIsRefreshing(true);
    try {
      await dispatch(productsActions.fetchProducts());
    } catch (error) {
      setError(error.message);
    }
    setIsRefreshing(false);
  }, [dispatch, setIsLoading, setError]);

  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', loadProducts);

    return () => {
      unsubscribe();
    };

  }, [loadProducts]);

  useEffect(() => { // if put async here => return a promise so shouldn't put it here
    setIsLoading(true);
    loadProducts().then(() =>{
      setIsLoading(false);
    });
  }, [dispatch, loadProducts]);

  const selectItemHandler = (id, title) => {
    props.navigation.navigate('ProductDetail', { productId: id, productTitle: title });
  };

  if(error) {
    return (
      <View style={styles.center}>
        <Text>An error occured. Message: {error}</Text>
        <Button title="Try again" onPress={loadProduct} color={Colors.primary} />
      </View>
    );
  }

  if(isLoading) {
    return (
      <View style={styles.center}>
        <ActivityIndicator
          size="large"
          color={Colors.primary} />
      </View>
    );
  }

  if (!isLoading && products.length === 0) {
    return (
      <View style={styles.center}>
        <Text>No products founds. Maybe start adding some !</Text>
      </View>
    )
  }

  return (
    <FlatList
      onRefresh={loadProducts}
      refreshing={isRefreshing}
      data={ products }
      keyExtractor={item => item.id}
      renderItem={itemData => (
        <ProductItem
          image={itemData.item.imageUrl}
          title={itemData.item.title}
          price={itemData.item.price}
          onSelect={() => selectItemHandler(itemData.item.id, itemData.item.title)}
        >
          <Button color={Colors.primary} title="View Details" onPress={() => selectItemHandler(itemData.item.id, itemData.item.title)} />
          <Button color={Colors.primary} title="To Cart" onPress={() => {
            dispatch(cartActions.addToCard(itemData.item));
          }} />
        </ProductItem>
      )} />
  );
}

export const screenOptions = navData => {
  return {
    headerTitle : 'All Products',
    headerRight: () => (<HeaderButtons HeaderButtonComponent={HeaderButton}>
      <Item title="Cart" iconName={ Platform.OS === 'android' ? 'md-cart' : 'ios-cart' } onPress={() => {
        navData.navigation.navigate('CartScreen');
      }}></Item>
    </HeaderButtons>),
    headerLeft: () => (<HeaderButtons HeaderButtonComponent={HeaderButton}>
      <Item title="Menu" iconName={ Platform.OS === 'android' ? 'md-menu' : 'ios-menu' } onPress={() => {
        navData.navigation.toggleDrawer();
      }}></Item>
    </HeaderButtons>)
  }
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default ProductsOverviewScreen;