import React, { useEffect } from 'react';
import { StatusBar, Platform } from 'react-native';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { AppLoading } from 'expo';
import { useFonts } from 'expo-font';
import { composeWithDevTools } from 'redux-devtools-extension';
import ReduxThunk from 'redux-thunk';
import * as Notifications from 'expo-notifications';
import * as Permissions from 'expo-permissions';

import OpenSans from './assets/fonts/OpenSans-Regular.ttf';
import OpenSansBold from './assets/fonts/OpenSans-Bold.ttf';

import productsReducer from './store/reducers/products';
import cartReducer from './store/reducers/cart';
import ordersReducer from './store/reducers/orders';
import authReducer from './store/reducers/auth';
import Appnavigator from './navigation/Appnavigator';

Notifications.setNotificationHandler({
  handleNotification: async () => {
    return {
       shouldShowAlert: true
    };
  }
});

const rootReducer = combineReducers({
  products: productsReducer,
  cart: cartReducer,
  orders: ordersReducer,
  auth: authReducer
});

const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

export default function App() {

  useEffect(() => {
    Permissions.getAsync(Permissions.NOTIFICATIONS)
    .then((statusObj) => {
      if(statusObj.status !== 'granted') {
        return Permissions.askAsync(Permissions.NOTIFICATIONS);
      }
      return statusObj;
    })
    .then((statusObj) => {
      if(statusObj.status !== 'granted') {
        throw new Error('Permission not granted!');
      }
    })
    // .then(() => {
    //   return Notifications.getExpoPushTokenAsync();
    // })
    // .then(response => {
    //   const token = response.data;
    //   setPushToken(token);
    // })
    .catch(error => {
      return null;
    });
  }, []);

  let [fontLoaded] = useFonts({
    'open-sans': OpenSans,
    'open-sans-bold': OpenSansBold
  });

  if(!fontLoaded) {
    return <AppLoading />
  }

  return (
    <Provider store={store}>
      <Appnavigator />
      <StatusBar barStyle={ Platform.OS === 'android' ? 'default' : 'dark-content'} />
    </Provider>
  );
}
