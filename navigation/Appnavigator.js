import React from 'react';
import { useSelector } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

// import ShopNavigator from './ShopNavigator';
import { ShopNavigator, AuthNavigator } from './ShopNavigator';
import StartupScreen from '../screens/StartupScreen';


const AppNavigator = props => {

  // const navRef = useRef();
  const isAuth = useSelector(state => !!state.auth.token);
  const didTryAutoLogin = useSelector(state => !!state.auth.didTryAutoLogin);

  // useEffect(() => {
  //   if (!isAuth) {
  //     navRef.current.dispatch(NavigationActions.navigate({ routeName: 'Auth' }));
  //   }
  // }, [isAuth]);

  // return <ShopNavigator ref={navRef} />;
  return (
    <NavigationContainer>
      { isAuth && <ShopNavigator name="Shop" /> }
      { !isAuth && didTryAutoLogin && <AuthNavigator name="Auth" /> }
      { !isAuth && !didTryAutoLogin && <StartupScreen /> }
    </NavigationContainer>
  );
};

export default AppNavigator;