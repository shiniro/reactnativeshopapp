import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator, DrawerItemList } from '@react-navigation/drawer';
import { Platform, View, SafeAreaView, Button} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { useDispatch } from 'react-redux';

import ProductsOverviewScreen, { screenOptions as productsOverviewScreenOptions } from '../screens/shop/ProductsOverviewScreen';
import ProductDetailScreen, { screenOptions as productDetailsScreenOptions } from '../screens/shop/ProductDetailScreen';
import CartScreen, { screenOptions as cartScreenOptions } from '../screens/shop/CartScreen';
import OrderScreen, { screenOptions as orderScreenOptions } from '../screens/shop/OrdersScreen';

import UserProductsScreen, { screenOptions as userProductsScreenOptions } from '../screens/user/UserProductsScreen';
import EditProductScreen, { screenOptions as editProductsScreenOptions } from '../screens/user/EditProductScreen';

import StartupScreen from '../screens/StartupScreen';
import AuthScreen, { screenOptions as authScreenOptions } from '../screens/user/AuthScreen';

import * as AuthActions from '../store/actions/auth';
import Colors from '../constants/Colors';

const defaultNavOptions = {
  headerStyle: {
    backgroundColor: Platform.OS === 'android' ? Colors.primary : ''
  },
  headerTintColor: Platform.OS === 'android' ? 'white' : Colors.primary
};

const ProductStackNavigator = createStackNavigator();

export const ProductsNavigator = () => {
  return (
    <ProductStackNavigator.Navigator screenOptions={defaultNavOptions} >
      <ProductStackNavigator.Screen
        name="ProductsOverview"
        component={ProductsOverviewScreen} 
        options={productsOverviewScreenOptions}
        />
      <ProductStackNavigator.Screen 
        name="ProductDetail"
        component={ProductDetailScreen}
        options={productDetailsScreenOptions}
        />
      <ProductStackNavigator.Screen
        name="CartScreen"
        component={CartScreen}
        options={cartScreenOptions}
        />
    </ProductStackNavigator.Navigator>
  );
};

// const ProductsNavigator = createStackNavigator(
//   {
//     ProductsOverview: ProductsOverviewScreen,
//     ProductDetail: ProductDetailScreen,
//     CartScreen: CartScreen
//   }, {
//     navigationOptions: {
//       drawerIcon: drawerConfig => <Ionicons name={ Platform.OS === 'android' ? 'md-cart' : 'ios-list' } size={23} color={drawerConfig.tintColor} />
//     },
//     defaultNavigationOptions: defaultNavOptions
//   }
// );

const OrdersNavigatorStack = createStackNavigator();

const OrderNavigator = () => {
  return (
    <OrdersNavigatorStack.Navigator screenOptions={defaultNavOptions}>
      <OrdersNavigatorStack.Screen name="OrderScreen" component={OrderScreen} options={orderScreenOptions} />
    </OrdersNavigatorStack.Navigator>
  )
};

// const OrdersNavigator = createStackNavigator({
//   OrderScreen: OrderScreen
// }, {
//   navigationOptions: {
//     drawerIcon: drawerConfig => <Ionicons name={ Platform.OS === 'android' ? 'md-list' : 'ios-list' } size={23} color={drawerConfig.tintColor} />
//   },
//   defaultNavigationOptions: defaultNavOptions
// });

const AdminNavigatorStack = createStackNavigator();

const AdminNavigator = () => {
  return (
    <AdminNavigatorStack.Navigator screenOptions={defaultNavOptions}>
      <AdminNavigatorStack.Screen name="UserProductsScreen" component={UserProductsScreen} options={userProductsScreenOptions} />
      <AdminNavigatorStack.Screen name="EditProductScreen" component={EditProductScreen} options={editProductsScreenOptions} />
    </AdminNavigatorStack.Navigator>
  );
};
 
// const AdminNavigator = createStackNavigator({
//   UserProductsScreen: UserProductsScreen,
//   EditProductScreen: EditProductScreen
// }, {
//   navigationOptions: {
//     drawerIcon: drawerConfig => <Ionicons name={ Platform.OS === 'android' ? 'md-create' : 'ios-create' } size={23} color={drawerConfig.tintColor} />
//   },
//   defaultNavigationOptions: defaultNavOptions
// });

const ShopDrawerNavigator = createDrawerNavigator();

export const ShopNavigator = () => {
  const dispatch = useDispatch();

  return (
    <ShopDrawerNavigator.Navigator
      drawerContent={props => {
        return (
          <View style={{ flex: 1, paddingTop: 20 }}>
            <SafeAreaView forceInset={{ top: 'always', horizontal: 'never' }}>
              <DrawerItemList {...props} />
              <Button title="Logout" color={Colors.primary} onPress={() => {
                dispatch(AuthActions.logout());
                // props.navigation.navigate('Auth');
              }} />
            </SafeAreaView>
          </View>
        )
      }
      }
      drawerContentOptions={{activeTintColor: Colors.primary}}
    >
      <ShopDrawerNavigator.Screen
        name="Products"
        component={ProductsNavigator}
         options={{
          drawerIcon: drawerConfig => <Ionicons name={ Platform.OS === 'android' ? 'md-cart' : 'ios-list' } size={23} color={drawerConfig.tintColor} />
         }} 
        />
      <ShopDrawerNavigator.Screen
        name="Orders"
        component={OrderNavigator} 
        options={{
          drawerIcon: drawerConfig => <Ionicons name={ Platform.OS === 'android' ? 'md-list' : 'ios-list' } size={23} color={drawerConfig.tintColor} />
        }}
        />
      <ShopDrawerNavigator.Screen
        name="Admin"
        component={AdminNavigator}
        options={{
          drawerIcon: drawerConfig => <Ionicons name={ Platform.OS === 'android' ? 'md-create' : 'ios-create' } size={23} color={drawerConfig.tintColor} />
        }}
        />
    </ShopDrawerNavigator.Navigator>
  );
};

// const ShopNavigator = createDrawerNavigator({
//   Products: ProductsNavigator,
//   Orders: OrdersNavigator,
//   Admin: AdminNavigator
// }, {
//   contentOptions: {
//     activeTintColor: Colors.primary
//   },
//   contentComponent: props => {
//     const dispatch = useDispatch();

//     return (
//       <View style={{ flex: 1, paddingTop: 20 }}>
//         <SafeAreaView forceInset={{ top: 'always', horizontal: 'never' }}>
//           <DrawerItems {...props} />
//           <Button title="Logout" color={Colors.primary} onPress={() => {
//             dispatch(AuthActions.logout());
//             props.navigation.navigate('Auth');
//           }} />
//         </SafeAreaView>
//       </View>
//     )
//   }
// });

const AuthNavigatorStack = createStackNavigator();

export const AuthNavigator = () => {
  return (
    <AuthNavigatorStack.Navigator screenOptions={defaultNavOptions}>
      <AuthNavigatorStack.Screen name="AuthScreen" component={AuthScreen} options={authScreenOptions} />
    </AuthNavigatorStack.Navigator>
  );
};

// const AuthNavigator = createStackNavigator({
//   AuthScreen: AuthScreen, 
// }, {
//   defaultNavigationOptions: defaultNavOptions
// })

// const MainNavigator = createSwitchNavigator({
//   Startup: StartupScreen,
//   Auth: AuthNavigator,
//   Shop: ShopNavigator
// });

// export default createAppContainer(MainNavigator);