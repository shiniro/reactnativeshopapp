import * as Notifications from 'expo-notifications';
import Product from '../../models/product';

export const DELETE_PRODUCT = 'DELETE_PRODUCT';
export const CREATE_PRODUCT = 'CREATE_PRODUCT';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';
export const SET_PRODUCTS = 'SET_PRODUCTS'

export const fetchProducts = () => {
  return async (dispatch, getState) => {
    const userId = getState().auth.userId;
    // can excute any async code here
    try {
      const response = await fetch('https://rn-complete-guide-dd900.firebaseio.com/products.json');

      if(!response.ok) {
        throw new Error('Something went wrong!');
      }

      const resData = await response.json();
      console.log(resData);

      const loadedProducts = [];
      for (const key in resData) {
        loadedProducts.push(new Product(key, resData[key].ownerId, resData[key].ownerPushToken, resData[key].title, resData[key].imageUrl, resData[key].description, resData[key].price));
      }
      
      dispatch({
        type: SET_PRODUCTS,
        products: loadedProducts,
        userProducts: loadedProducts.filter(prod => prod.ownerId === userId)
      })
    } catch (error) {
      // send to custom analytics server
      throw error;
    }
  };
};

export const deleteProduct = productId => {
  return async (dispatch, getState) => {
    const token = getState().auth.token;
    const response = await fetch(`https://rn-complete-guide-dd900.firebaseio.com/products/${productId}.json?auth=${token}`, {
      method: 'DELETE',
    });

    if(!response.ok) {
      throw new Error('Something went wrong !');
    }

    dispatch({ type: DELETE_PRODUCT, productId: productId });
  };
};

export const createProduct = (title, description, imageUrl, price) => {

  return async (dispatch, getState) => {

    let pushToken = await Notifications.getExpoPushTokenAsync();

    console.log(pushToken);

    if(pushToken) {
      pushToken = pushToken.data;
      const token = getState().auth.token;
      const userId = getState().auth.userId;

      // can excute any async code here
      const response = await fetch(`https://rn-complete-guide-dd900.firebaseio.com/products.json?auth=${token}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          title,
          description,
          imageUrl,
          price,
          ownerId: userId,
          ownerPushToken: pushToken
        })
      });

      const resData = await response.json();

      dispatch({
        type: CREATE_PRODUCT,
        productData: {
          id: resData.name,
          title,
          description,
          imageUrl,
          price,
          ownerId: userId,
          pushToken: pushToken
        }
      })
    } else {
      throw new Error('Something went wrong with the token !');
    }
  };
}

export const updateProduct = async (id, title, description, imageUrl) => {
  return async (dispatch, getState) => {
      const token = getState().auth.token;
      const response = await fetch(`https://rn-complete-guide-dd900.firebaseio.com/products/${id}.json?auth=${token}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        title,
        description,
        imageUrl
      })
    });

    if(!response.ok) {
      throw new Error('Something went wrong !');
    }

    dispatch({
      type: UPDATE_PRODUCT,
      productId: id,
      productData: {
        title,
        description,
        imageUrl
      }
    })
  }
}